import axios, {AxiosResponse} from 'axios';

const instance = axios.create({
    baseURL: '/',
    timeout: 1000,
    headers: {
        'Accept': 'application/json'
    }
});


type ServerResponse = {
    result: number;
}

type OperationMap = {
    [key: string]: string,
}


const operationMap: OperationMap = {
    "+": "add",
    "-": "subtract",
    "*": "multiply",
    "/": "divide",
};



const performCalculation = async (first: string, operation: string, second: string): Promise<AxiosResponse> => {
    const url: string = `${operationMap[operation]}/${first}/${second}`;

    const request = await instance.get<ServerResponse>(url);
    return request;
}

export default performCalculation;