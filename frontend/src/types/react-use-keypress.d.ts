declare module 'react-use-keypress' {
    function useKeypress(keys: string | array, handler: function);
    export = useKeypress;
}