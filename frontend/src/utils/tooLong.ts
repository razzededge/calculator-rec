function tooLong(value: string | null, maxLength: number = 10): boolean {
    if(value === null){
        return false;
    }

    return value.length >= maxLength;
}

export default tooLong;