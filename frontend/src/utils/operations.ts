type Operations = {
    [key: string]: string
};

const operations: Operations  = {
    "+": "+",
    "*": "∗",
    "/": "∕",
    "-": "-"
}

export default operations;