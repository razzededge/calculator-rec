function flipSign(value: string): string {
    const spl = value.split('-');
    return spl.length === 1 ? `-${spl[0]}` : spl[1];
}

export default flipSign;