function alreadyHasStartingZeros(value: string | null): boolean {
  if(value === null){
    return false;
  }

  const str = value.split("-");
  return (
    (str.length === 1 && str[0] === "0") || (str.length === 2 && str[1] === "0")
  );
}

export default alreadyHasStartingZeros;
