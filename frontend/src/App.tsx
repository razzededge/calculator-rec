import "./App.css";

import Calculator from "./components/Calculator/Calculator";

function App() {
  return (
    <>
      <div className="text-center pt-10 max-w-sm mx-auto">
        <Calculator />
      </div>
      <footer className="pt-20 mx-auto text-center text-gray-500">
        <div className="text-sm">
          These are only active if launched from api server
        </div>
        <div>
          OpenAPI spec: <a href="./openapi.json">/openapi.json</a>
        </div>
        <div>
          API Explorer: <a href="./explorer">/explorer</a>
        </div>
      </footer>
    </>
  );
}

export default App;
