const buttonMain: string = `font-semibold hover:bg-opacity-80 cursor-pointer text-2xl rounded-full h-20 flex items-center justify-center`

const buttonTopLine: string = `bg-gray-300 text-black w-20`;
const buttonNumbers: string = `bg-gray-500 text-white w-20`;
const buttonZero: string = `bg-gray-500 text-white w-44`;
const buttonOperations: string = `bg-yellow-500 text-white w-20`

export {
    buttonMain,
    buttonTopLine,
    buttonNumbers,
    buttonZero,
    buttonOperations
};