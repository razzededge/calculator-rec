import { useState } from "react";
import useKeypress from 'react-use-keypress';

import flipSign from "../../utils/flipSign";
import isFloat from "../../utils/isFloat";
import alreadyHasStartingZeros from "../../utils/alreadyHasStartingZeros";
import tooLong from "../../utils/tooLong";
import operations from "../../utils/operations";

import performCalculation from "../../services/calculator";

import Display from "../Display/Display";
import {
  buttonMain,
  buttonTopLine,
  buttonNumbers,
  buttonOperations,
  buttonZero,
} from "./styleDefs";

function Calculator() {
  const [first, setFirst] = useState<string | null>(null);
  const [second, setSecond] = useState<string | null>(null);
  const [result, setResult] = useState<number | null>(null);
  const [operation, setOperation] = useState<string | null>(null);

  //1 - first number, 2 operation, 3 second number1
  const [processStep, setProcessStep] = useState(1);


  const reset = (withResult: boolean) => (ev: any) => {
    setFirst(null);
    setSecond(null);
    setOperation(null);
    setProcessStep(1);

    if (withResult) {
      setResult(null);
    }
  };

  const getResult = async () => {
    if (processStep === 3) {
      const result = await performCalculation(
        first as string,
        operation as string,
        second as string
      );
      if (result) {
        const { data } = result;
        reset(false)(false);
        setResult(data.result);
      }
    }
  };

  const handleSolution = (valueClicked: number) => (ev: any) => {
    if (processStep === 1) {
      if (alreadyHasStartingZeros(first) || tooLong(first)) {
        return;
      }

      if (first === null) {
        setFirst(`${valueClicked}`);
        setResult(null);
      } else {
        setFirst(`${first}${valueClicked}`);
      }
    }

    if (processStep === 2) {
      setSecond(`${valueClicked}`);
      setProcessStep(3);
    }

    if (processStep === 3) {
      if (alreadyHasStartingZeros(second) || tooLong(second)) {
        return;
      }

      setSecond(`${second}${valueClicked}`);
    }
  };

  const changeSign = () => {
    if (processStep === 1 && first !== null) {
      setFirst(flipSign(first));
    }

    if (processStep === 3 && second !== null) {
      setSecond(flipSign(second));
    }
  };

  const handleOperation = (operationClicked: string) => (ev: any) => {
    if ((processStep === 1 && first !== null) || processStep === 2) {
      setOperation(operationClicked);
      setProcessStep(2);
    }
  };

  const makeFloat = () => {
    if (processStep === 1 && first !== null && !isFloat(first)) {
      setFirst(`${first}.`);
    }
    if (processStep === 1 && first === null) {
      setFirst(`0.`);
    }
    if (processStep === 2 && second === null) {
      setSecond(`0.`);
    }
    if (processStep === 3 && second !== null && !isFloat(second)) {
      setSecond(`${second}.`);
    }
  };

  const handlePercentage = async () => {
    if(processStep === 1 && first !== null) {
      const result = await performCalculation(
        first,
        '/',
        '100'
      );
      if (result) {
        const { data } = result;
        if(data.result !== 0){
          setFirst((data.result).toString());
        }
        
      }
  
    }

    if(processStep === 3 && second !== null) {
      const result = await performCalculation(
        second,
        '/',
        '100'
      );
      if (result) {
        const { data } = result;
        if(data.result !== 0){
          setSecond((data.result).toString());
        }

      }
    }
  }

  useKeypress(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'], (event: any) => {
    const pressed: number = parseInt(event.key, 10);
    handleSolution(pressed)(null);
  });

  useKeypress(['-', '+', '*', '/'], (event: any) => {
    handleOperation(event.key)(null);
  });

  useKeypress(['Escape', 'Backspace'], (event: any) => {
    reset(true)(null);
  });

  useKeypress('Enter', (event: any) => {
    getResult();
  });

  useKeypress([',', '.'], (event: any) => {
    makeFloat();
  });


  return (
    <>
      <Display
        first={first as string}
        operation={operation as string}
        second={second as string}
        result={result as number}
      />
      <div className="grid grid-cols-4 grid-rows-6 gap-2">
        <div className={`${buttonMain} ${buttonTopLine}`} onClick={reset(true)}>
          C
        </div>
        <div className={`${buttonMain} ${buttonTopLine}`} onClick={changeSign}>
          -/+
        </div>
        <div className={`${buttonMain} ${buttonTopLine}`} onClick={handlePercentage}>%</div>
        <div
          className={`${buttonMain} ${buttonOperations}`}
          onClick={handleOperation("/")}
        >
          {operations["/"]}
        </div>

        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(9)}
        >
          9
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(8)}
        >
          8
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(7)}
        >
          7
        </div>
        <div
          className={`${buttonMain} ${buttonOperations}`}
          onClick={handleOperation("*")}
        >
          {operations["*"]}
        </div>

        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(6)}
        >
          6
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(5)}
        >
          5
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(4)}
        >
          4
        </div>
        <div
          className={`${buttonMain} ${buttonOperations}`}
          onClick={handleOperation("-")}
        >
          {operations["-"]}
        </div>

        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(3)}
        >
          3
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(2)}
        >
          2
        </div>
        <div
          className={`${buttonMain} ${buttonNumbers}`}
          onClick={handleSolution(1)}
        >
          1
        </div>
        <div
          className={`${buttonMain} ${buttonOperations}`}
          onClick={handleOperation("+")}
        >
          {operations["+"]}
        </div>

        <div
          className={`${buttonMain} ${buttonZero}  col-span-2`}
          onClick={handleSolution(0)}
        >
          0
        </div>
        <div className={`${buttonMain} ${buttonNumbers}`} onClick={makeFloat}>
          .
        </div>
        <div
          className={`${buttonMain} ${buttonOperations}`}
          onClick={getResult}
        >
          =
        </div>
      </div>
    </>
  );
}

export default Calculator;
