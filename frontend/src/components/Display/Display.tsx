import React from "react";
import tooLong from "../../utils/tooLong";
import operations from "../../utils/operations";

import "./Display.css";

type DisplayProps = {
  first: string;
  operation: string;
  second: string;
  result: number;
};

const Display = ({ first, operation, second, result }: DisplayProps) => {
  let chain: string = `${first || ""} ${operations[operation] || ""} ${second || ""} ${
    result || ""
  }`;
  chain = chain.trim();
  const split = tooLong(chain, 13);

  const textSize = result && tooLong(result.toString(), 17) ? 'text-3xl pr-8 leading-10 pb-0' : 'text-5xl pr-6'

  return (
    <div className={`pt-6 text-right display-height bg-yell ${textSize}`}>
      {split ? (
        <>
          <div className="text-lg">
            {first} {operations[operation]}
          </div>
          {second} {result}
        </>
      ) : (
        chain
      )}
    </div>
  );
};

export default Display;
