# Calculator


## How to run

### Prerequisites

Latest LTS Node 16.x (Loopback will complain if non-lts version is used by  WARN EBADENGINE in console)

And frontend will fail with Error: error:0308010C:digital envelope routines::unsupported

(I will provide compiled frontend just for the convenience).

I tested and developed this on Ubuntu 20.4, unfortunatelly I had not time to test run&build on windows or osx

### Steps

Prefered launch steps:

- in folder "backend" run `npm install`
- in folder "frontend" run `npm install`
- in folder "frontend" run `npm run build`
- copy all files from folder `frontend/build` do `backend/public`, overwrite if needed
- in folder "backend" run `npm run start`

Loopback dev server will start at default port 3000

Calculator will be under root (http://localhost:3000/) (there are links at the bottom to explore the API)

## Important Notes

- % was implemented like on apple calculator on ios device: it will divide value by 100 if used and any further 


- I have decided to follow calculator familiarity in terms of UX so instead of adding a digit after = is pressed and we get result, the calculator will start from the begining and the digit pressed will be the start of first number of equation ie: 1 + 2 = 3 and we press 9 we will not have 39 but 9 future_operation etc... This might sound controversial - as this is how Windows and Android Calculator works - in the presented movie a person used Apple Calculator behavior as an example and it works like the one adding digit to result. I think it is better to target general population unless we have a specific use case in mind.



<center>
According to statscounter.com, current market share global:

|OS   |Share   |
|---|---|
|Android    |39.75%|
|Windows    |32.44%|
|iOS        |16.7%|
|OS X       |6.85%|
|Unknown    |1.9%|
|Chrome OS  |1.1%|

</center>

- There is no AC button, C always clears all
- If you start as first digit with 0 this will result in only possible next actions to be `C` `.` or one of the `/*-+`
- You can use computer keyboard to enter numbers and perform operations, only negative/positive sign change isn't supported, supported keys `1234567890 ., -+*/ Enter Escape Backspace`
| `Enter` is used to get result instead of `=` key  | `Escape` / `Backspace` the same as button `C` on interface
| `,` & `.` behave the same... but there might be some glitches, not tested enough.
- There is not much done in terms of unit tests - no time, only partially covering api
- Frontend will not display any visible to user errors if something fails with api
