import {param, get} from '@loopback/rest';
import {Decimal} from 'decimal.js';

/* lets be honest there is no way javascript is safe for floating point
    0.1 + 0.2 will get 0.30000000000000004
    200.30 * 3  will get 600.9000000000001
    0.99999999999999999 will be rounded to 1
    and 
    0.9999999999999999 * Math.pow(10,16) will result in 9999999999999998
    this is why I decided to use arithmetic library
*/

const precision: number = 20;
const minE: number = -7;

Decimal.set({ precision, minE });

export type CalculatorOperationResult = {
  result: number;
};

export class CalculatorController {
  private static responseSchema: object = {
    schema: {
      type: 'object',
      properties: {
        result: {
          type: 'number',
          format: 'double',
        },
      },
    },
  };

  @get('/add/{firstAddend}/{secondAddend}', {
    summary: 'Adds two numbers',
    responses: {
      '200': {
        description: 'OK',
        content: {
          'application/json': CalculatorController.responseSchema,
        },
      },
    },
  })
  add(
    @param.path.number('firstAddend') firstAddend: number,
    @param.path.number('secondAddend') secondAddend: number,
  ): CalculatorOperationResult {
    return {
      result: new Decimal(firstAddend).plus(secondAddend).toNumber(),
    };
  }

  @get('/divide/{dividend}/{divisor}', {
    summary: 'Divides 2 numbers',
    responses: {
      '200': {
        description: 'OK',
        content: {
          'application/json': CalculatorController.responseSchema,
        },
      },
    },
  })
  divide(
    @param.path.number('dividend') dividend: number,
    @param.path.number('divisor') divisor: number,
  ): CalculatorOperationResult {
    return {
      result: new Decimal(dividend).dividedBy(divisor).toNumber(),
    };
  }

  @get('/subtract/{minuend}/{subtrahend}', {
    summary: 'Substracts two numbers',
    responses: {
      '200': {
        description: 'OK',
        content: {
          'application/json': CalculatorController.responseSchema,
        },
      },
    },
  })
  substract(
    @param.path.number('minuend') minuend: number,
    @param.path.number('subtrahend') subtrahend: number,
  ): CalculatorOperationResult {
    return {
      result: new Decimal(minuend).minus(subtrahend).toNumber(),
    };
  }

  @get('/multiply/{multiplier}/{multiplicand}', {
    summary: 'Multiplies two numbers',
    responses: {
      '200': {
        description: 'OK',
        content: {
          'application/json': CalculatorController.responseSchema,
        },
      },
    },
  })
  multiply(
    @param.path.number('multiplier') multiplier: number,
    @param.path.number('multiplicand') multiplicand: number,
  ): CalculatorOperationResult {
    return {
      result: new Decimal(multiplier).times(multiplicand).toNumber(),
    };
  }

  // @get('/percentage/{}/{}', {
  //   summary: 'Multiplies two numbers',
  //   responses: {
  //     '200': {
  //       description: 'OK',
  //       content: {
  //         'application/json': CalculatorController.responseSchema,
  //       },
  //     },
  //   },
  // })
  // percentage(
  //   @param.path.number('multiplier') multiplier: number,
  //   @param.path.number('multiplicand') multiplicand: number,
  // ): CalculatorOperationResult {
  //   return {
  //     result: new Decimal(multiplier).times(multiplicand).toNumber(),
  //   };
  // }

}
