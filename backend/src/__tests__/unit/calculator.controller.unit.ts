/* eslint-disable @typescript-eslint/no-explicit-any */

import {expect} from '@loopback/testlab';

import {CalculatorController} from '../../controllers/calculator.controller';

describe('[Unit] CalculatorController', () => {
  const controller = new CalculatorController();

  describe('Entering other data types than numbers should result in error', () => {
    it('Should not allow anything other than numbers in add()', async () => {
      const sum = () => controller.add('test' as any, 'tes1' as any).result;
      expect(sum).to.throw();
    });
    it('Should not allow strings in substract()', async () => {
      const sub = () =>
        controller.substract('test' as any, 'tes1' as any).result;
      expect(sub).to.throw();
    });
    it('Should not allow strings in divide()', async () => {
      const div = () =>
        controller.divide('test' as any, 'tes1' as any).result;
      expect(div).to.throw();
    });
    it('Should not allow strings in multiply()', async () => {
      const mul = () =>
        controller.substract('test' as any, 'tes1' as any).result;
      expect(mul).to.throw();
    });
  });

  describe('Adding numbers:', () => {
    describe('should be able to sum 2 positive numbers', () => {
      it('adds 2 and 3 to equal 5', async () => {
        const sum = controller.add(2, 3).result;
        expect(sum).to.equal(5);
      });
      it('adds 0.1 and 0.2 to equal 0.3', async () => {
        const sum = controller.add(0.1, 0.2).result;
        expect(sum).to.not.equal(0.30000000000000004);
        expect(sum).to.equal(0.3);
      });
      it('adds 1023 and 0.999999999 to equal 1023.999999999', async () => {
        const sum = controller.add(1023, 0.999999999).result;
        expect(sum).to.equal(1023.999999999);
      });
      it('adds 0.99999999999999999 and 0 to equal 0.99999999999999999', async () => {
        const sum = controller.add(0.99999999999999999, 0).result;
        expect(sum).to.equal(0.99999999999999999);
      });
      it('adds 999999999999999999999 and 1000000 to equal 1.000000000000001e+21', async () => {
        const sum = controller.add(999999999999999999999, 1000000).result;
        expect(sum).to.equal(1.000000000000001e21);
      });
    });
    describe('should be able to sum 2 different sign numbers', () => {
      it('adds 1 and -1 to equal 0', async () => {
        const sum = controller.add(1, -1).result;
        expect(sum).to.equal(0);
      });
      it('adds 0.1 and -0.01 should equal 0.09', async () => {
        const sum = controller.add(0.1, -0.01).result;
        expect(sum).to.not.equal(0.09000000000000001);
        expect(sum).to.equal(0.09);
      });
      it('adds 0.0000000000000000022 and -0.0000000000000000001 to equal 0.0000000000000000021', async () => {
        const sum = controller.add(
          0.0000000000000000022,
          -0.0000000000000000001,
        ).result;
        expect(sum).to.equal(0.0000000000000000021);
      });
    });
    describe('should be able to sum 2 negative numbers', () => {
      it('adds -1 and -1 to equal -2', async () => {
        const sum = controller.add(-1, -1).result;
        expect(sum).to.equal(-2);
      });
      it('adds -0.1 and -0.2 to equal -0.3', async () => {
        const sum = controller.add(-0.1, -0.2).result;
        expect(sum).to.equal(-0.3);
      });
    });
  });

  describe('Substracting numbers:', () => {
    describe('should be able to subtract 2 positive numbers', () => {
      it('subtracting 1 from 2 to be equal 1', async () => {
        const sub = controller.substract(2, 1).result;
        expect(sub).to.equal(1);
      });
    });
    describe('should be able to substract 2 mixed sign numbers', () => {
      it('substracts -1000000 with -99999999999999 to equal', async () => {
        const sub = controller.substract(-1000000, -99999999999999).result;
        expect(sub).to.equal(99999998999999);
      });
    });
    describe('should be able to subtract 2 negative numbes', () => {});
  });

  describe('Multiplying numbers:', () => {
    describe('should be able to multiply 2 positive numbers', () => {});
    describe('should be able to multiply 2 mixed sign numbers', () => {});
    describe('should be able to multiply 2 negative numbers', () => {});
  });

  describe('Dividing numbers:', () => {
    describe('should handle division by 0 properly', () => {});
    describe('should be able to divide 2 positive numbers', () => {});
    describe('should be able to divide 2 mixed sign numbers', () => {});
    describe('should be able to divide 2 negative numbers', () => {});
  });
});
